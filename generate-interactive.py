from statblock import Statblock

# TODO: attacks, actions, and abilities

print("Please enter the monster statistics...")
name = input("  Name: ")
_str = input("  Strength: ")
_dex = input("  Dexterity: ")
_con = input("  Constitution: ")
_int = input("  Intelligence: ")
_wis = input("  Wisdom: ")
_cha = input("  Charisma: ")
cr = input("  Challenge Rating: ")
hit_die = input("  Hit Die: ")
size = input("  Size: ")
category = input("  Monster Type: ")
alignment = input("  Alignment: ")
speed = input("  Speed: ")
armor_class = input("  Armor Class: ")
skills = input("  Skills: ")
dmg_res = input("  Damage Resistances: ")
dmg_imm = input("  Damage Immunities: ")
cnd_imm = input("  Condition Immunities: ")
senses = input("  Senses: ")
languages = input("  Languages: ")
"""
abilities = []
selection = input("Do you want to add abilities (y/n)? ")
finished = True if selection == 'n' else False
while not finished:
    a_name = input("  Name: ")
    a_desc = input("  Description: ")
    a_tuple = (a_name, a_desc)
    abilities.append(a_tuple)
    selection = input("Are you done adding abilities (y/n)? ")
    finished = True if selection == 'y' else False

actions = []
selection = input("Do you want to add actions (y/n)? ")
finished = True if selection == 'n' else False
while not finished:
    a_name = input("  Name: ")
    a_desc = input("  Description: ")
    a_tuple = (a_name, a_desc)
    actions.append(a_tuple)
    selection = input("Are you done adding actions (y/n)? ")
    finished = True if selection == 'y' else False
"""
sb = Statblock(name, _str, _dex, _con, _int, _wis, _cha, cr,
               hit_die, size, category, alignment, speed, armor_class,
               skills, dmg_res, dmg_imm, cnd_imm, senses, languages,
               abilities, actions)

sb.make_statblock()
output_file = input("Enter the filename for the statblock (end it with .html): ")
sb.write_to_html(output_file)
