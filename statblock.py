import math
import template

# TODO: Actually store action and property info instead of just the template string?
#       Store as dicts? or lists?

hit_die_lookup = {"1d4": (2,3),
                  "1d6": (3,4),
                  "1d8": (4,5),
                  "1d10": (5,6),
                  "1d12": (6,7),
                  "1d20": (10,11)}

STATBLOCK_TEMPLATE = """

<stat-block>
    <creature-heading>
      <h1>{name}</h1>
      <h2>{size} {category}, {alignment}</h2>
    </creature-heading>

    <top-stats>
      <property-line>
        <h4>Armor Class</h4>
        <p>{armor_class}</p>
      </property-line>
      <property-line>
        <h4>Hit Points</h4>
        <p>{hit_points}</p>
      </property-line>
      <property-line>
        <h4>Speed</h4>
        <p>{speed}</p>
      </property-line>
      <abilities-block data-cha="{data_cha}" data-con="{data_con}" data-dex="{data_dex}" data-int="{data_int}" data-str="{data_str}" data-wis="{data_wis}"></abilities-block>
      <property-line>
        <h4>Skills</h4>
        <p>{skills}</p>
      </property-line>
      <property-line>
        <h4>Senses</h4>
        <p>{senses}</p>
      </property-line>
      <property-line>
        <h4>Languages</h4>
        <p>{languages}</p>
      </property-line>
      <property-line>
        <h4>Challenge</h4>
        <p>{challenge}</p>
      </property-line>
{properties}
    </top-stats>

{abilities}
    
    <h3>Actions</h3>
    
{actions}
    
  </stat-block>
"""

HIT_POINT_TEMPLATE = "{total} ({cr}{hit_die} + {con})"

PROPERTY_TEMPLATE = """    <property-line>
        <h4>{name}</h4>
        <p>{description}</p>
    </property-line>"""

CHALLENGE_TEMPLATE = "{cr} ({xp} XP)"

ABILITY_TEMPLATE = """    <property-block>
        <h4>{name}.</h4>
        <p>{description}</p>
    </property-block>"""

ACTION_TEMPLATE = """    <property-block>
        <h4>{name}.</h4>
        <p>{description}</p>
    </property-block>"""

ATTACK_TEMPLATE = """    <property-block>
        <h4>{name}.</h4>
        <p><i>{a_type}:</i> +{attack} to hit, {range}, {targets}. <i>Hit:</i> {damage} {d_type} damage.</p>
    </property-block>"""


class Statblock:
    def __init__(self, name, _str, _dex, _con, _int, _wis, _cha,
                 challenge_rating, hit_die, size, category, alignment,
                 speed, armor_class, skills, senses, languages,
                 hit_points=None, dmg_res=None, dmg_imm=None, cnd_imm=None,
                 attacks=[], actions=[], abilities=[]):
        """Initialize the statblock with everything except actions and abilities."""
        self.name = name
        self._str = int(_str)
        self._dex = int(_dex)
        self.initiative = self.modifier(self._dex)
        self._con = int(_con)
        self._int = int(_int)
        self._wis = int(_wis)
        self._cha = int(_cha)
        self.score_lookup = {"str": self._str,
                             "dex": self._dex,
                             "con": self._con,
                             "int": self._int,
                             "wis": self._wis,
                             "cha": self._cha}
        # CR is used for display and LEVEL is used for math
        self.cr = str(challenge_rating)
        self.level = 1 if '/' in self.cr else int(self.cr)
        self.prof_bonus = math.ceil(self.level / 4) + 1
        self.xp = 0 # TODO: xp lookup based on CR
        self.hit_die = hit_die.lower()
        self.hit_die_value = hit_die_lookup[self.hit_die]
        math_hit_points = math.ceil(self.level / 2) * self.hit_die_value[0] + \
                          math.floor(self.level / 2) * self.hit_die_value[1] + \
                          self.level * self.modifier(self._con)
        self.hit_points = math_hit_points if hit_points is None else hit_points
        self.size = size
        self.category = category
        self.alignment = alignment
        self.speed = speed
        self.armor_class = armor_class
        self.skills = "-" if not skills else skills
        self.senses = "-" if not senses else senses
        self.languages = "-" if not languages else languages
        self.abilities = abilities
        self.attacks = attacks
        self.actions = actions
        self.properties = []
        if dmg_res:
            self.add_property("Damage Resistances", dmg_res)
        if dmg_imm:
            self.add_property("Damage Immunities", dmg_imm)
        if cnd_imm:
            self.add_property("Condition Immunities", cnd_imm)
        self.statblock = None

    @staticmethod
    def modifier(value):
        return math.floor((value - 10) / 2)

    def add_attack(self, name, damage_type, damage_dice, num_targets,
                   attack_range, attack_ability, is_melee_attack=False):
        """Add attack action."""
        attack_type = "Melee Weapon Attack" if is_melee_attack else "Ranged Weapon Attack"
        ability_modifer = self.modifier(self.score_lookup[attack_ability])
        attack_modifer = ability_modifer + self.prof_bonus
        range_string = "reach " if is_melee_attack else "range "
        range_string = range_string + str(attack_range)
        target_ending = " target" if int(num_targets) == 1 else " targets"
        target_string = str(num_targets) + target_ending
        damage_dice_key = "1" + damage_dice[1:]
        d_one, d_two = hit_die_lookup[damage_dice_key]
        num_dice = int(damage_dice[0])
        damage_average = math.ceil(num_dice / 2) * d_one + \
                         math.floor(num_dice / 2) * d_two + \
                         ability_modifer
        damage_string = f"{damage_average} ({damage_dice} + {ability_modifer})"
        
        attack_string = ATTACK_TEMPLATE.format(name=name, a_type=attack_type,
                                               attack=attack_modifer, range=range_string,
                                               targets=target_string, damage=damage_string,
                                               d_type=damage_type)
        self.actions.append(attack_string)

    def add_action(self, name, description):
        """Add (non-attack) action."""
        action_string = ACTION_TEMPLATE.format(name=name, description=description)
        self.actions.append(action_string)

    def add_ability(self, name, description):
        """Add ability."""
        ability_string = ABILITY_TEMPLATE.format(name=name, description=description)
        self.abilities.append(ability_string)

    def add_property(self, name, description):
        """Add property."""
        property_string = PROPERTY_TEMPLATE.format(name=name, description=description)
        self.properties.append(property_string)

    def make_statblock(self):
        """Create the statblock html string."""
        abilities_string = "\n".join(self.abilities)
        actions_string = "\n".join(self.actions)
        properties_string = "\n".join(self.properties)
        hit_points_string = HIT_POINT_TEMPLATE.format(total=self.hit_points,
                                                      cr=self.level,
                                                      hit_die=self.hit_die[1:],
                                                      con=str(self.modifier(self._con) * self.level))
        challenge_string = CHALLENGE_TEMPLATE.format(cr=self.cr, xp=self.xp)
        self.statblock = STATBLOCK_TEMPLATE.format(name=self.name,
                                                   size=self.size,
                                                   category=self.category,
                                                   alignment=self.alignment,
                                                   armor_class=self.armor_class,
                                                   hit_points=hit_points_string,
                                                   speed=self.speed,
                                                   data_cha=self._cha,
                                                   data_con=self._con,
                                                   data_dex=self._dex,
                                                   data_int=self._int,
                                                   data_str=self._str,
                                                   data_wis=self._wis,
                                                   skills=self.skills,
                                                   senses=self.senses,
                                                   languages=self.languages,
                                                   challenge=challenge_string,
                                                   properties=properties_string,
                                                   abilities=abilities_string,
                                                   actions=actions_string)

    def write_to_html(self, output_file=None):
        """Write out the html string to the given file."""
        if self.statblock is None:
            raise Exception("Cannot write to HTML - statblock has not been made")
        output_file = f"{self.name}.html" if output_file is None else output_file
        output_html = template.string.format(name=self.name,
                                             statblock=self.statblock)
        with open(output_file, 'w') as html_file:
            html_file.write(output_html)

def init_statblock_from_dict(args):
    """Wrapper to create a Statblock object from a dictionary of params."""
    hit_points = args.get("hit points", None)
    dmg_res = args.get("dmg res", None)
    dmg_imm = args.get("dmg imm", None)
    cnd_imm = args.get("cnd imm", None)
    sb = Statblock(args["name"], args["str"], args["dex"], args["con"], args["int"], args["wis"], args["cha"],
                   args["challenge"], args["hit die"], args["size"], args["category"], args["alignment"],
                   args["speed"], args["armor class"], args["skills"], args["senses"], args["languages"],
                   hit_points, dmg_res, dmg_imm, cnd_imm)
    attacks = args.get("attacks", [])
    for a in attacks:
        is_melee = True if a["attack type"] == "melee" else False
        sb.add_attack(a["name"], a["damage type"], a["damage dice"], a["num targets"],
                      a["range"], a["ability"], is_melee)
    actions = args.get("actions", [])
    for a in actions:
        sb.add_action(a["name"], a["description"])
    abilities = args.get("abilities", [])
    for a in abilities:
        sb.add_ability(a["name"], a["description"])
    return sb