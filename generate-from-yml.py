import yaml
from statblock import init_statblock_from_dict

"""Expected YML format:

name: Fight Boy
str: 18
dex: 16
con: 14
int: 9
wis: 11
cha: 8
challenge: 4
hit die: 1d10
hit points: 35                          # optional
size: medium
category: humanoid (any)
alignment: any
speed: 30
armor class: 16 (breastplate)
skills: Athletics +4, Perception +4
dmg res: pyschic                        # optional
dmg imm: fire                           # optional
cnd imm: frightened                     # optional
senses: passive Perception 12
languages: Common and one other
attacks:
  - name: Longsword
    damage type: slashing
    damage dice: 1d10
    num targets: 1
    range: 5 feet
    ability: str
    attack type: melee
actions:
  - name: Action Surge (recharges after a rest)
    description: Fight Boy can take two actions this turn.
abilities:
  - name: Soldier's Vigilance
    description: Fight Boy cannot be suprised.
"""

input_file = "statblock.yml"

input_dict = None
with open(input_file, 'r') as yaml_file:
    input_dict = yaml.load(yaml_file)

sb = init_statblock_from_dict(input_dict)
sb.make_statblock()
sb.write_to_html()
