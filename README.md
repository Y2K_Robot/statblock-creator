DnD 5e Statblock Creator
========================

This project aims to provide an easy way to generate slick looking statblocks for DnD monsters
for Fifth Edition. The output of the scripts is an HTML file that can be viewed using any
web browser and is based on the HTML templates from this project:

https://github.com/Valloric/statblock5e